<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Finance_Email_Alert</fullName>
        <description>Finance Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>luis_ernesto_sr@hotmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RB_Robotics_Templates/Finance_Account_Creation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Stage_to_Close_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Set Stage to Close Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Negotiation</fullName>
        <field>StageName</field>
        <literalValue>Negotiation/Review</literalValue>
        <name>Set Stage to Negotiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Waiting_for_approval</fullName>
        <field>StageName</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Stage Waiting for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
